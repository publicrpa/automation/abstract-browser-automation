package publicrpa.automation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import publicrpa.view.AbstractBrowserView;

public abstract class AbstractBrowserAutomation extends AbstractAutomation {

    public AbstractBrowserAutomation(String jsonString) {
		super(jsonString);
	}

	@Override
    protected void finalizeFlow() {
        if (AbstractBrowserView.driver != null) {
        	AbstractBrowserView.driver.quit();
        }
    }

    @Override
    protected void beforeFlow() {
    	WebBrowserDriver.initialize(config);
    }

	static String readConfigFile(String[] args) throws IOException {
		return Files.lines(Paths.get(args[0])).collect(Collectors.joining());
	}

	@Override
	protected void handleExpection(Exception e) {
		e.printStackTrace();
	}

}
