package publicrpa.automation;

import java.util.HashMap;
import java.util.function.Function;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public enum BrowserType {

	CHROME((headlessMode) -> {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=pl-PL");
        options.addArguments("--remote-allow-origins=*");
        if (headlessMode) {
        	options.addArguments("--headless=new");
        }
    	HashMap<String, Object> preference = new HashMap<>();
        preference.put("useAutomationExtension", false);
        preference.put("intl.accept_languages", "pl");
        options.setExperimentalOption("prefs", preference);
		return new ChromeDriver(options);
	}),
	FIREFOX((headlessMode) -> {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("intl.accept_languages", "pl");
        profile.setPreference("dom.webnotifications.enabled", false);
        FirefoxOptions options = new FirefoxOptions();
        if (headlessMode) {
        	options.addArguments("--headless=new");
        }
        options.setProfile(profile);
		return new FirefoxDriver(options);
	}),
	EDGE((headlessMode) -> {
        final var options = new EdgeOptions();
        options.setAcceptInsecureCerts(true);
		final var driver = new EdgeDriver(options);
		return driver;
	});

	private Function<Boolean, WebDriver> initFunction;

	private BrowserType(Function<Boolean, WebDriver> f) {
		this.initFunction = f;
	}

	public WebDriver initialize(Boolean settings) {
		WebDriver driver = initFunction.apply(settings);
		driver.manage().deleteAllCookies();
		return driver;
	}

}
