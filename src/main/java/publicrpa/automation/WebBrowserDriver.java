package publicrpa.automation;

import publicrpa.view.AbstractBrowserView;

public class WebBrowserDriver {

	public static void initialize(Configuration config) {
        String browserName = config.get("browserName");
        String headlessMode = config.get("headlessMode");
        BrowserType browser = BrowserType.valueOf(browserName);
        AbstractBrowserView.driver = browser.initialize(Boolean.parseBoolean(headlessMode));
	}

}
